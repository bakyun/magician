package magician

import (
	"bytes"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/magefile/mage/mg"
)

const (
	currDir = "."
)

var (
	isWin = runtime.GOOS == "windows"
)

func execute(env map[string]string, wd string, stdout, stderr io.Writer, cmd string, args ...string) error {
	var err error

	platformCmd := cmd
	platformCmd, err = exec.LookPath(cmd)
	if err != nil {
		platformCmd, err = exec.LookPath(filepath.Join(wd, cmd))
	}
	if err == nil {
		platformCmd, err = filepath.Abs(platformCmd)
	}
	if err != nil {
		return err
	}

	c := exec.Command(platformCmd, args...)
	c.Env = os.Environ()
	for k, v := range env {
		c.Env = append(c.Env, k+"="+v)
	}
	c.Stderr = stderr
	c.Stdout = stdout
	c.Stdin = os.Stdin
	c.Dir, err = filepath.Abs(wd)
	if err != nil {
		return err
	}

	return c.Run()
}

// ExecEx provides all options to customize the exec call
func ExecEx(env map[string]string, wd string, cmd string, args ...string) error {
	EchoCommand(cmd, args...)
	err := execute(env, wd, os.Stdout, os.Stderr, cmd, args...)
	EchoCommandResult(err)

	return err
}

// ExecOutput runs the sepcified command and returns its output
func ExecOutput(cmd string, args ...string) (string, error) {
	if mg.Verbose() {
		EchoCommand(cmd, args...)
	}

	buf := &bytes.Buffer{}
	err := execute(nil, currDir, buf, os.Stderr, cmd, args...)
	if mg.Verbose() {
		EchoCommandResult(err)
	}

	if err != nil {
		return "", err
	}

	return strings.TrimSuffix(buf.String(), "\n"), nil
}

// Exec runs the specified command
func Exec(cmd string, args ...string) error {
	EchoCommand(cmd, args...)
	err := execute(nil, currDir, os.Stdout, os.Stderr, cmd, args...)
	EchoCommandResult(err)

	return err
}

// ExecSilent runs the specified command, ditching all output
func ExecSilent(cmd string, args ...string) error {
	EchoCommand(cmd, args...)
	err := execute(nil, currDir, nil, os.Stderr, cmd, args...)
	EchoCommandResult(err)

	return err
}
